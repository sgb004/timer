/**
 * @name Timer;
 * @version 1.0.4
 * @author sgb004
 */

class Timer {
	#timer;
	#callback = () => {};
	#isRunning = false;
	#time = 100;
	#delay = 0;

	constructor(callback, time, delay) {
		if (typeof callback !== 'function') {
			throw new Error('The parameter is not a function.');
		} else {
			this.#callback = callback;
		}

		if (time) {
			time = parseInt(time);
			if (isNaN(time)) {
				throw new Error('The time must be an int.');
			} else {
				this.#time = time;
			}
		}

		if (delay) {
			delay = parseInt(delay);
			if (isNaN(delay)) {
				throw new Error('The delay must be an int.');
			} else {
				this.#delay = delay;
			}
		}
	}

	#execute = () => this.#callback();

	start() {
		if (!this.#isRunning) {
			const startInterval = () => (this.#timer = setInterval(this.#execute, this.#time));

			if (this.#delay === 0) {
				startInterval();
			} else {
				const executeAfterDelay = () => {
					clearTimeout(this.#timer);
					startInterval();
				};

				this.#timer = setTimeout(executeAfterDelay, this.#delay);
			}

			this.#isRunning = true;
		}
	}

	stop() {
		clearInterval(this.#timer);
		this.#isRunning = false;
	}

	reset() {
		this.stop();
		this.start();
	}

	/**
	 * @param {number} time
	 */
	set time(time) {
		time = parseInt(time);
		if (isNaN(time)) {
			throw new Error('The time must be an int.');
		} else {
			this.#time = time;
		}
	}

	get time() {
		return this.#time;
	}

	changeTime(time) {
		this.time = time;
		this.reset();
	}
}

export default Timer;
