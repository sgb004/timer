declare class Timer {
	constructor(callback: () => void, time?: number, delay?: number);

	start(): void;
	stop(): void;
	reset(): void;
	time: number;
	changeTime(time: number): void;
}

export default Timer;
