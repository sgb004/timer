import Timer from './timer.js';

describe('Timer', () => {
	test('should throw error if callback is not a function', () => {
		expect(() => {
			new Timer('not a function');
		}).toThrowError();
	});

	test('should throw error if time is not a number', () => {
		expect(() => {
			new Timer(() => {}, 'not a number');
		}).toThrowError();
	});

	test('should throw error if delay is not a number', () => {
		expect(() => {
			new Timer(() => {}, 100, 'not a number');
		}).toThrowError();
	});

	test('should start and call callback in 100ms without specifying a time', () => {
		jest.useFakeTimers();
		const callback = jest.fn();
		const timer = new Timer(callback);

		timer.start();

		expect(callback).not.toHaveBeenCalled();

		jest.advanceTimersByTime(100);
		expect(callback).toHaveBeenCalledTimes(1);

		timer.stop();
	});

	test('should start and call callback repeatedly', () => {
		jest.useFakeTimers();
		const callback = jest.fn();
		const timer = new Timer(callback, 100);

		timer.start();

		expect(callback).not.toHaveBeenCalled();

		jest.advanceTimersByTime(100);
		expect(callback).toHaveBeenCalledTimes(1);

		jest.advanceTimersByTime(100);
		expect(callback).toHaveBeenCalledTimes(2);

		timer.stop();
	});

	test('should delay initial callback if delay provided', () => {
		jest.useFakeTimers();
		const callback = jest.fn();
		const timer = new Timer(callback, 100, 1000);

		timer.start();

		expect(callback).not.toHaveBeenCalled();

		jest.advanceTimersByTime(500);
		expect(callback).not.toHaveBeenCalled();

		jest.advanceTimersByTime(600);
		expect(callback).toHaveBeenCalledTimes(1);

		timer.stop();
	});

	test('should reset timer if startTimer called while running', () => {
		jest.useFakeTimers();
		const callback = jest.fn();
		const timer = new Timer(callback, 100);

		timer.start();

		expect(callback).not.toHaveBeenCalled();

		jest.advanceTimersByTime(100);
		expect(callback).toHaveBeenCalledTimes(1);

		jest.advanceTimersByTime(50);
		timer.reset();

		expect(callback).toHaveBeenCalledTimes(1);

		jest.advanceTimersByTime(50);
		expect(callback).toHaveBeenCalledTimes(1);

		jest.advanceTimersByTime(50);
		expect(callback).toHaveBeenCalledTimes(2);

		timer.stop();
	});

	test('should not update timer interval when time is changed using set time', () => {
		jest.useFakeTimers();
		const callback = jest.fn();
		const timer = new Timer(callback, 100);

		timer.start();

		jest.advanceTimersByTime(100);
		expect(callback).toHaveBeenCalledTimes(1);

		timer.time = 200;

		jest.advanceTimersByTime(200);
		expect(callback).toHaveBeenCalledTimes(3);

		timer.stop();
	});

	test('should update timer interval when time is changed using changeTime function', () => {
		jest.useFakeTimers();
		const callback = jest.fn();
		const timer = new Timer(callback, 100);

		timer.start();

		jest.advanceTimersByTime(100);
		expect(callback).toHaveBeenCalledTimes(1);

		timer.changeTime(200);

		jest.advanceTimersByTime(200);
		expect(callback).toHaveBeenCalledTimes(2);

		timer.stop();
	});
});
