# Timer

This is a class that allows you to create a simple timer in JavaScript. You can use this class to execute a callback function periodically after a specified time interval.

## Installation

To install the `Timer` you can use npm:

```node
npm i sgb004-timer
```

## Usage

To use it, simply create a new `Timer` instance passing in the callback function, interval in milliseconds, and optionally an initial delay:

<!-- prettier-ignore -->
```js
const timer = new Timer(() => { console.log('¡Hola!'); }, 1000, 5000);
```

This will execute the callback function every second, after an initial 5 seconds delay.

Then to start the timer, call the `start()` method:

```js
timer.start();
```

And to stop it, use `stop()`:

```js
timer.stop();
```

You can also restart the timer with `reset()`:

```js
timer.reset();
```

## API

### Constructor

-   `callback` - Function to execute on each interval.
-   `time` - (Optional) Interval in milliseconds.
-   `delay` - (Optional) Initial delay in milliseconds before starting.

### Methods

-   `start()` - Starts the timer execution.
-   `stop()` - Stops the timer.
-   `reset()` - Restarts the timer.

## Author

[sgb004](https://sgb004.com)

## License

MIT
